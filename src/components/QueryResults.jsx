import React from "react";

export default function QueryResults({ data }) {
  // const rows = data.followups.map(followup =>{

  return (
    <div style={{width: "50%"}}>
      <h2>BEST:</h2>
      <div>
        <span>Category:</span> {data.best.text}
      </div>
      <div>
        <span>Score:</span> {data.best.score}
      </div>
      {data.followups.length ? 
      <>
      <h3>Other bests</h3>
      <table className="queryTable">
        <thead>
          <tr>
            <th>Category</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>{data.followups.map((followup) => {
            return <tr><td>{followup.text}</td><td>{followup.score}</td></tr>
        })}</tbody>
      </table></>: null}
      
    </div>
  );
}
