
import { useState } from 'react';
import './App.css';
import QueryResults from './components/QueryResults';

function App() {
  const [text, setText] = useState('');
  const [error, isError] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const [loading, isLoading] = useState(false);
  const [data, setData] = useState({});

  const handleChange = (e)=>{
    setText(e.target.value)
  }

  const onSubmit = (e)=>{
    e.preventDefault();
   
    const getData = async () =>{
      try {
        isError(false);
        isLoading(true);
        setErrorMsg('');
        if(!text.length) return;
        const encodedTxt = encodeURIComponent(text);
        const data = await fetch(`https://batonleads.herokuapp.com/api/beta/category/?lookup=${encodedTxt}`);

        const response = await data.json();

        if( data.status !== 200){
          throw response;
        }

        console.log(response);
        setData(response)
        setText('');
        isLoading(false);
      } catch (error) {
        console.error('error', error);
        isError(true);
        setErrorMsg(error.message);
        setText('');
        isLoading(false);
        setData({})
      }
    }
    getData();
  }

  return (
    <div className="App">
      <div className='Form'>
      <form>
        <label>
          Query:
            <input type="text" name="name" id='search' value={text} onChange={handleChange} />
        </label>
        <input type="submit" value="Submit" onClick={onSubmit} />

      </form>

      </div>
       { error ? <p>{errorMsg}</p> : ""}
       { loading ? <div class="loader"></div> : ""}

       {Object.keys(data)?.length ? <QueryResults data={data} />: ""}
    </div>
  );
}

export default App;
